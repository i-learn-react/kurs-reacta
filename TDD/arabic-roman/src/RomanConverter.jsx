import React from 'react';
import { toRoman, } from './toRoman';

class RomanConverter extends React.Component {
    state = {
        roman: null,
    }

    handleChange = (event) => {
        const arabic = parseInt(event.target.value);
        this.setState({ roman: toRoman(arabic) })
    }

    render() {
        return (
            <div>
                <label htmlFor='arabicValue'>
                    Arabic Value:
                    <input name='arabicValue' type='number' onChange={this.handleChange}/>
                </label>
                <h2>Result: {this.state.roman}</h2>
            </div>
        );
    }
}

export default RomanConverter;