import React from 'react';
import RomanAdder from './RomanAdder';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('RomanAdder test suite', () => {
    it('should display two inputs', () => {
        render(<RomanAdder />);
        const roman1 = screen.getByLabelText(/roman number 1/i);
        const roman2 = screen.getByLabelText(/roman number 2/i);
        user.type(roman1, 'I');
        user.type(roman2, 'I');
        expect(screen.getByText(/result: ii/i));
    });
});