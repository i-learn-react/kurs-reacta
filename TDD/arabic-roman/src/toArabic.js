import { ARABIC_ROMAN_PARTIALS, } from './constants';

const findPartialIn = roman =>
    partial =>
        roman.startsWith(partial.roman);

const getRemainingRoman = (roman, position) => roman.substring(position);

let result = 0;

export const getArabicValue = roman => {
    const findInRoman = findPartialIn(roman);
    const partial = ARABIC_ROMAN_PARTIALS.find(findInRoman);
    const restOfRoman = getRemainingRoman(roman, partial.roman.length);
    return (restOfRoman.length > 0)
        ? partial.arabic + getArabicValue(restOfRoman)
        : partial.arabic;

}

export const toArabic = roman => getArabicValue(roman);