import React from 'react';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import ArabicConverter from './ArabicConverter';

describe('ArabicConverter', () => {
    it('should return none for default', () => {
        render(<ArabicConverter />);
        const romanInput = screen.getByLabelText(/roman/i);
        expect(screen.getByText(/result: none/i))
    });
    
    it('should return 1 for "I"', () => {
        render(<ArabicConverter />);
        const romanInput = screen.getByLabelText(/roman/i);
        user.type(romanInput, 'I');
        expect(screen.getByText(/result: 1/i))
    });

    it('should return 2019 for "MMXIX"', () => {
        render(<ArabicConverter />);
        const romanInput = screen.getByLabelText(/roman/i);
        user.type(romanInput, 'MMXIX');
        expect(screen.getByText(/result: 2019/i))
    });
});