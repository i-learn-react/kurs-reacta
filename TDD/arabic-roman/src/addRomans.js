import { toArabic, } from './toArabic';
import { toRoman, } from './toRoman';

export const addRomans = (roman1, roman2) => toRoman(toArabic(roman1) + toArabic(roman2));
