import React, { Fragment } from 'react';

import RomanConverter from './RomanConverter';
import ArabicConverter from './ArabicConverter';
import RomanAdder from './RomanAdder';
import './App.css';

const App = () => (
    <Fragment>
        <RomanConverter />
        <ArabicConverter />
        <RomanAdder />
    </Fragment>
)

export default App;
