import { toArabic, } from './toArabic';

const testCases = [
    ['none', 0],
    ['I', 1],
    ['IV', 4],
    ['V', 5],
    ['VI', 6],
    ['IX', 9],
    ['X', 10],
    ['XI', 11],
];
    
describe('toArabic test suite', () => {
    test.each(testCases)(
        'converts %s to %i',
        (roman, expectedArabic) => expect(toArabic(roman)).toBe(expectedArabic)
    )
    test('should convert XIV to 14', () => {
        expect(toArabic('XIV')).toBe(14);
    });
});