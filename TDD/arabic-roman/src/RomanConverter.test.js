import React from 'react';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';

import RomanConverter from './RomanConverter';

describe('Roman Converter test suite', () => {
    it('has an input field', () => {
        render(<RomanConverter />);
        const arabicInput = screen.getByLabelText(/arabic/i);
    });
    it('returns "none" for 0', () => {
        render(<RomanConverter />);
        const arabicInput = screen.getByLabelText(/arabic value/i);
        user.type(arabicInput, '0');
        expect(() => {
            screen.getByText(/result: none/i);
        }).not.toThrow();
    });

    it('converts 1 to I', () => {
        render(<RomanConverter />)
        const arabicInput = screen.getByLabelText(/arabic value/i);
        user.type(arabicInput, '1');
        expect(() => {
            screen.getByText(/result: i/i);
        }).not.toThrow();
    });
    it('converts 2019 to MMXIX', () => {
        render(<RomanConverter />)
        const arabicInput = screen.getByLabelText(/arabic value/i);
        user.type(arabicInput, '2019');
        expect(() => {
            screen.getByText(/result: mmxix/i);
        }).not.toThrow();
    });
});