import { addRomans, } from './addRomans';

describe('addRomans test suite', () => {
    test.each([
        ['I', 'I', 'II'],
        ['I', 'II', 'III'],
        ['II', 'II', 'IV'],
        ['I', 'IV', 'V'],
        ['V', 'V', 'X'],
        ['C', 'D', 'DC'],
        ['M', 'M', 'MM'],
        ['XIX', 'MM', 'MMXIX'],
    ])(
        'add %s to %s should give %s',
        (roman1, roman2, expectedSum) => expect(addRomans(roman1, roman2)).toBe(expectedSum)
    );
});