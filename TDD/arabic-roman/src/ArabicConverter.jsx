import React, { Component } from 'react';
import { toArabic, } from './toArabic';

class ArabicConverter extends Component {
    state = {
        arabic: null,
    }

    handleChange = (event) => this.setState({ arabic: toArabic(event.target.value) });

    render() {
        const { arabic } = this.state;
        return (
            <div>
                <label htmlFor='romanInput'>
                    Roman number
                     <input type='text' onChange={this.handleChange} />
                </label>
                <h2>Result: { arabic ? arabic : 'none' }</h2>
            </div>
        );
    }
}

export default ArabicConverter;