import React from 'react';
import { addRomans, } from './addRomans';

class RomanAdder extends React.Component {
    state = {
        roman1: 'none',
        roman2: 'none',
    }

    handleChange = id => event => {
        this.setState({
            [id]: event.target.value,
        });
    }

    render() {
        const { roman1, roman2 } = this.state;
        return (
            <div>
                <label htmlFor='roman1'>
                    <span>Roman number 1:</span>
                    <input
                        type='text'
                        id='roman1'
                        value={roman1}
                        onChange={this.handleChange('roman1')}
                    />
                </label>    
                <label htmlFor='roman2'>
                    <span>Roman number 2:</span>
                    <input
                        type='text'
                        id='roman2'
                        value={roman2}
                        onChange={this.handleChange('roman2')}
                    />
                </label>    
                <h2>Result: {addRomans(roman1, roman2)}</h2>
            </div>
        );
    };
};

export default RomanAdder;