import { toRoman, getRomanValue } from './toRoman';

describe('toRoman test suite', () => {
    const testCases = [
        [0, 'none'],
        [1, 'I'],
        [4, 'IV'],
        [5, 'V'],
        [6, 'VI'],
        [9, 'IX'],
        [10, 'X'],
        [11, 'XI'],
    ];

    describe('should convert arabic to roman', () => {
        test.each(testCases)(
            'converts %i into %s',
            (arabic, expectedRoman) => expect(toRoman(arabic)).toBe(expectedRoman)
        );
    });

    describe('getArabicPartial function', () => {
        it('should return [`V`, `I`] for number 6', () => {
            expect(getRomanValue(6)).toEqual(['V', 'I']);
        });
        it('should return [`X`, `X`, `V`, `I`] for number 26', () => {
            expect(getRomanValue(26)).toEqual(['X', 'X', 'V', 'I']);
        });
        it('should return [`M`, `M`, `X`, `IX`] for number 26', () => {
            expect(getRomanValue(2019)).toEqual(['M', 'M', 'X', 'IX']);
        });
    });
    
});