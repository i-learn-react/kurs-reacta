import { ARABIC_ROMAN_PARTIALS, } from './constants';

const subtractFrom = (total, number) => total - number;

const findClosestRomanPartial = number => ARABIC_ROMAN_PARTIALS.find(partial => partial.arabic <= number)

export const getRomanValue = (total) => {
    const nextPartial = findClosestRomanPartial(total);
    const newTotal = subtractFrom(total, nextPartial.arabic);
    if (newTotal > 0) {
        return [nextPartial.roman, ...getRomanValue(newTotal)];
    }
    if (newTotal === 0) {
        return [nextPartial.roman];
    }
}

export const toRoman = arabic => getRomanValue(arabic).join('');