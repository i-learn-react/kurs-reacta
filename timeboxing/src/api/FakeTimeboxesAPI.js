import uuid from 'uuid';

function wait(ms = 1000) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const timeboxes = [{
    id: 'a1234',
    title: 'Nie wiem co teraz',
    totalTimeInMinutes: '25',
}, {
    id: 'b1234',
    title: 'A teraz wiem!',
    totalTimeInMinutes: '15',
}, {
    id: 'c1234',
    title: 'To ja',
    totalTimeInMinutes: '20',
    }];

const FakeTimeboxesAPI = {
    getAllTimeboxes: async function() {
        await wait(1000);
        return [...timeboxes];
    },
    addTimebox: async function(newTimebox) {
        await wait(1000);
        const timeboxAdded = { ...newTimebox, id: uuid.v4() };
        timeboxes.push(timeboxAdded);
        return timeboxAdded;
    },
    replaceTimebox: async function(indexToReplace) {
        await wait(1000);
        const timeboxToReplace = { ...timeboxes[indexToReplace] };
        if (!timeboxToReplace) {
            throw new Error('Timebox does not exist!');
        }
        timeboxes[indexToReplace] = {...timeboxToReplace, title: 'updated Timebox title'};
        return timeboxes;
    },
    deleteTimebox: async function (indexToDelete) {
        if (!timeboxes[indexToDelete]) {
            throw new Error('Timebox does not exist!');
        }
        timeboxes.splice(indexToDelete, 1);
        return [...timeboxes];
    }
}

export default FakeTimeboxesAPI;