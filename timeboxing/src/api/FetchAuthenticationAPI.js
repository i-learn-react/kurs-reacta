import makeRequest from './makeFetchRequest';
const fetch = (url, options) => window.fetch(url, { ...options });

export default function createAuthenticationAPI(options = { baseUrl: 'http://localhost:3001'}) {
    return FetchAuthenticationAPI(options.baseUrl);
}

const FetchAuthenticationAPI = BASE_URL => ({
    login: async function (credentials) {
        const response = await makeRequest(
            `${BASE_URL}/login`,
            'POST',
            credentials
        );
        const token = await response.json();
        return token;
    },
});