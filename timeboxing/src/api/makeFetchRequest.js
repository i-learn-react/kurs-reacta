async function makeRequest(url, method, body, accessToken) {
    const headers = {
        'Content-Type': 'application/json',
    };
    if (accessToken) {
        headers['Authorization'] = `Bearer ${accessToken}`;
    }
    const jsonBody = body ? JSON.stringify(body) : undefined;
    const response = await window.fetch(
        url,
        {
            method: method,
            headers,
            body: jsonBody,
        }
    );
    if (!response.ok) {
        throw new Error('We can not add new timebox right now. Try again…');
    }
    return response;
}

export default makeRequest;