import makeRequest from './makeFetchRequest';
const fetch = (url, options) => window.fetch(url, { ...options });

export default function createTimeboxesAPI(options = { baseUrl: 'http://localhost:3001/timeboxes'}) {
    return FetchTimeboxesAPI(options.baseUrl);
}

const FetchTimeboxesAPI = BASE_URL => ({
    getAllTimeboxes: async function (accessToken) {
        const response = await makeRequest(BASE_URL, 'GET', null, accessToken)
        const timeboxes = await response.json();
        return [...timeboxes];
    },
    addTimebox: async function (newTimebox, accessToken) {
        const response = await makeRequest(BASE_URL, 'POST', newTimebox, accessToken);
        const timeboxAdded = await response.json();
        return timeboxAdded;
    },
    replaceTimebox: async function (updatedTimebox, accessToken) {
        const response = await makeRequest(
            `${BASE_URL}/${updatedTimebox.id}`,
            'PUT',
            updatedTimebox,
            accessToken,
        );
        const timeboxEdited = await response.json();
        return timeboxEdited;
    },
    partiallyUpdateTimebox: async function (timeboxToUpdate, accessToken) {
        const response = await makeRequest(
            `${BASE_URL}/${timeboxToUpdate.id}`,
            'PATCH',
            timeboxToUpdate,
            accessToken,
        );
        const timeboxUpdated = await response.json();
        return timeboxUpdated;
    },
    getTimeboxesByFullTextSearch: async function (searchText, accessToken) {
        const response = await makeRequest(
            `${BASE_URL}?q=${searchText}`,
            'GET',
            null,
            accessToken,
        )
        const timeboxesFound = await response.json();
        return timeboxesFound;
    },
    deleteTimebox: async function (timeboxToDelete, accessToken) {
        const response = await makeRequest(
            `${BASE_URL}/${timeboxToDelete.id}`,
            'DELETE',
            null,
            accessToken,
        );
    }
});