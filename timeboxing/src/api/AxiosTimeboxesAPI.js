import axios from "axios";

export default function createTimeboxesAPI(options = { baseUrl: 'http://localhost:3001/timeboxes'}) {
    return AxiosTimeboxesAPI(options.baseUrl);
}

const getHeaders = (accessToken) =>
    accessToken
        ? {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        }
        : {
            'Content-Type': 'application/json',
        };

const AxiosTimeboxesAPI = BASE_URL => ({
    getAllTimeboxes: async function (accessToken) {
        const response = await axios.get(
            BASE_URL,
            { headers: getHeaders(accessToken) },
        );
        return response.data;
    },
    addTimebox: async function (newTimebox, accessToken) {
        const response = await axios.post(
            BASE_URL,
            { ...newTimebox },
            { headers: getHeaders(accessToken) },
        );
        return response.data;
    },
    replaceTimebox: async function (updatedTimebox, accessToken) {
        const response = await axios.put(
            `${BASE_URL}/${updatedTimebox.id}`,
            { ...updatedTimebox },
            { headers: getHeaders(accessToken) },
        );
        return response.data;
    },
    partiallyUpdateTimebox: async function (timeboxToUpdate, accessToken) {
        const response = await axios.patch(
            `${BASE_URL}/${timeboxToUpdate.id}`,
            { ...timeboxToUpdate },
            { headers: getHeaders(accessToken) },
        );
        return response.data;
    },
    getTimeboxesByFullTextSearch: async function (searchText, accessToken) {
        const response = await axios.get(
            `${BASE_URL}?q=${searchText}`,
            { headers: getHeaders(accessToken) },
        );
        return response.data;
    },
    deleteTimebox: async function (timeboxToDelete, accessToken) {
        await axios.delete(
            `${BASE_URL}/${timeboxToDelete.id}`,
            { headers: getHeaders(accessToken) },
        );
    }
});