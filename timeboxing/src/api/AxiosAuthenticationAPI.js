import axios    from "axios";

export default function createAuthenticationAPI(options = { baseUrl: 'http://localhost:3001'}) {
    return AxiosAuthenticationAPI(options.baseUrl);
}

const AxiosAuthenticationAPI = BASE_URL => ({
    login: async function (credentials) {
        const response = await axios.post(
            `${BASE_URL}/login`,
            {...credentials},
        );
        
        return response.data;
    },
});