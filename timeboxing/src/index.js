import React from 'react';
import ReactDOM from 'react-dom';

import './Styles/styles.scss';
import App from './Components/App';

const root = document.getElementById('root');

ReactDOM.render(<App />, root);
