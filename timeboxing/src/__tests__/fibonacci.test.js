import { fib } from '../Helpers/fibonacci';

describe('Fibonacci test suite', () => {
    it('returns the same value for number smaller or equal 1', () => {
        expect(fib(0)).toBe(0);
        expect(fib(1)).toBe(1);
        expect(fib(-2)).toBe(-2);
        expect(fib('-1')).toBe('-1');
    });

    it('returns for values higher than 1', () => {
        expect(fib(2)).toBe(1);
        expect(fib(2)).toBe(fib(0) + fib(1));
        expect(fib(3)).toBe(fib(1)+fib(2));
    });
});