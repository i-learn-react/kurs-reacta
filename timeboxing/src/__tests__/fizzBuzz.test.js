import { fizzBuzz } from '../Helpers/fizzBuzz';

describe('FizzBuzz test suite', () => {
    describe('Returns undefined', () => {
        test('when no params passed', () => {
            expect(fizzBuzz()).toBe(undefined);
        });

    });

    describe('Returns Fizz', () => {
        test('when number can be divided by 3', () => {
            expect(fizzBuzz(3)).toBe('Fizz');
        });

        test('when negative number can be divided by 3', () => {
            expect(fizzBuzz(-3)).toBe('Fizz');
        });

        test('when string with number can be divided by 3', () => {
            expect(fizzBuzz('-3')).toBe('Fizz');
        });

    });

    describe('Returns Buzz', () => {
        test('when number can be divided by 5', () => {
            expect(fizzBuzz(5)).toBe('Buzz');
        });    
        
        test('when negative number can be divided by 5', () => {
            expect(fizzBuzz(5)).toBe('Buzz');
        });    

        test('when string with number can be divided by 5', () => {
            expect(fizzBuzz('-5')).toBe('Buzz');
        });
    });
    
    describe('Returns FizzBuzz', () => {
        test('when number can be divided by  3 and 5', () => {
            expect(fizzBuzz(15)).toBe('FizzBuzz');
        });    

        test('when number can be divided by  3 and 5', () => {
            expect(fizzBuzz(15)).toBe('FizzBuzz');
        });    

        test('when string with number can be divided by 3 and 5', () => {
            expect(fizzBuzz('-15')).toBe('FizzBuzz');
        });

        test('null value', () => {
            expect(fizzBuzz(null)).toBe('FizzBuzz');
        });
    });
    
    describe('Returns the same value as given', () => {
        test('for numbers that can not be divided by 3 or 5 ', () => {
            expect(fizzBuzz(1)).toBe(1);
        });

        test('for negative numbers that can not be divided by 3 or 5 ', () => {
            expect(fizzBuzz(-7)).toBe(-7);
        });

        test('for string, boolean', () => {
            expect(fizzBuzz('ala ma kota')).toBe('ala ma kota');
            expect(fizzBuzz(true)).toBe(true);
            expect(fizzBuzz(undefined)).toBe(undefined);
        });

    });
});