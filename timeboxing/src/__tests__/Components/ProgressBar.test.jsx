import React from 'react';
import ReactDOM from 'react-dom';
import TestRenderer from 'react-test-renderer';

import { ProgressBar } from '../../Components/ProgressBar';

let root;
let renderer;

describe('ProgressBar test suite', () => {
    describe('with default props (React Test Renderer)', () => {
        beforeEach(() => {
            renderer = TestRenderer.create(<ProgressBar />);
        });

        it('should match snapshot', () => {
            expect(renderer.toJSON()).toMatchSnapshot();
        });

        it('should be "DIV" element', () => {
            expect(renderer.toJSON().type).toMatch(/div/);
        });

        it('should have "progress" class name', () => {
            expect(renderer.toJSON().props.className).toMatch(/progress/);
        });
    });

    describe('with defined props (React Test Renderer)', () => {
        beforeEach(() => {
            renderer = TestRenderer.create(<ProgressBar percent={25} color='red' isBig className='foo' />);
        });

        it('should have additional "foo" class name', () => {
            expect(renderer.toJSON().props).toMatchObject(
                { "className": expect.stringMatching(/foo/) }
            )
        });

        it('should have "progress--big" class name', () => {
            expect(renderer.toJSON().props.className).toMatch(/progress--big/);
        });

        it('should have progress bar with style: width equal 25', () => {
            expect(renderer.toJSON().children[0].props.className).toMatch(/progress__bar/);
        });
    });

    describe('with default props (DOM)', () => {
        beforeEach(() => {
            root = document.createElement('div');
            ReactDOM.render(<ProgressBar />, root);
        });

        it('should return ProgressBar with default props', () => {
            expect(<ProgressBar />).toEqual(
                <ProgressBar className="" color="orange" isBig={false} isPaused={true} percent={0} trackRemaining={false} />
            );
        });

        it('should be "DIV" element', () => {
            expect(root.childNodes[0].nodeName).toEqual('DIV');
        });

        it('should have "progress" class name', () => {
            expect(root.childNodes[0].className).toMatch(/progress/);
        });
    });

    describe('with defined props (DOM)', () => {
        beforeEach(() => {
            root = document.createElement('div');
            ReactDOM.render(<ProgressBar percent={25} color='red' isBig className='foo' />, root);
        });

        it('should have additional "foo" class name', () => {
            expect(root.childNodes[0].className).toMatch(/foo/);
        });

        it('should have "progress--big" class name', () => {
            const progress = root.querySelector('.progress--big');
            expect(progress.className).toMatch(/progress--big/);
        });

        it('should have progress bar with style: width equal 25', () => {
            const progressBar = root.querySelector('.progress__bar');
            expect(progressBar.style.width).toMatch(/25%/);
        });
    });
});