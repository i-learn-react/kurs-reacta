import React from 'react';
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect'
import TimeboxList from '../../Components/TimeboxList';
import user from '@testing-library/user-event';

describe('TimeboxList test suite', () => {
    test('after fillings fields new timebox should be added', () => {
        render(<TimeboxList />);
        const whatInput = screen.getByLabelText(/co robisz/i);
        const timeInput = screen.getByLabelText(/ile minut/i);
        user.type(whatInput, 'Foo');
        user.type(timeInput, '10');
        user.click(screen.getByRole('button', '/dodaj timebox/i'));
        const timebox = screen.queryByText(/foo/i, 'h3')
        expect(timebox).toBeInTheDocument();
    });
});