import React from 'react';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';

import EditableTimebox from '../../Components/EditableTimebox';
import { TimeboxEditor } from '../../Components/TimeboxEditor';
import CurrentTimebox from '../../Components/CurrentTimebox';

const check = (element) => () => element;

describe('EditableTimebox test suite', () => {
    it('should allow enter edit mode, change title and accept changes', () => {
        render( <EditableTimebox /> );
        const editButton = screen.getByText('Edytuj');
        expect(check(editButton)).not.toThrow();
        user.click(editButton);
        const titleInput = screen.getByLabelText(/robisz/i);
        user.type(titleInput, 'Bar');
        const okButton = screen.getByText(/Zatwierdź/);
        expect(check(okButton)).not.toThrow();
        user.click(okButton);
        expect(check(screen.getByText('Bar'))).not.toThrow();
    });
});