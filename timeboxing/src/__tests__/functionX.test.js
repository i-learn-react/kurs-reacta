import { xxx } from '../Helpers/functionX';

describe('Function X test suite', () => {
    const error = (value) => () => xxx(value);

    it('should return reverted string', () => {
        expect(xxx('foo')).toBe('oof');
        expect(xxx('ala')).toBe('ala');
        expect(xxx('Ala')).toBe('alA');
    });

    it('returns error for nonstring values', () => {
        expect(error(null)).toThrow(TypeError);
        expect(error(1)).toThrow(TypeError);
        expect(error(undefined)).toThrow(TypeError);
        expect(error(true)).toThrow(TypeError);
        expect(error({})).toThrow(TypeError);
        expect(error([])).toThrow(TypeError);
    });
});