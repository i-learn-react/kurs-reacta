function increasePausesCount(count, isPaused) {
    return isPaused
        ? count
        : count + 1;
}

export { increasePausesCount };