const storage = window.localStorage;

export const saveAccessToken = (accessToken) => storage.setItem('accessToken', accessToken);

export const clearAccessToken = () => storage.removeItem('accessToken');

export const getAccessToken = () => storage.getItem('accessToken');

export const saveSessionEndtime = (endtime) => storage.setItem('sessionEndtime', endtime);

export const clearSessionEndtime = () => storage.removeItem('sessionEndtime');

export const getSessionEndtime = () => storage.getItem('sessionEndtime');