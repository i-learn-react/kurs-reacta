import React from 'react';
import PropTypes from 'prop-types';

import InlineText from './InlineText';

const SearchTimeboxes = ({handleSearch, handleSearchInput, handleClearSearch, searchText}) => {
    return (
        <form onSubmit={handleSearch} className='TimeboxSearch'>
            <label htmlFor="search-timebox">
                <InlineText >Search timeboxes with text: </InlineText>
                <input type='text' onChange={handleSearchInput} value={searchText} />
            </label>
            <button onClick={handleClearSearch} >x</button>
            <button>Search</button>
        </form>
    );
};

SearchTimeboxes.propTypes = {
    handleSearch: PropTypes.func.isRequired,
    handleSearchInput: PropTypes.func.isRequired,
    handleClearSearch: PropTypes.func.isRequired,
    searchText: PropTypes.string,
};

SearchTimeboxes.defaultProps = {
    searchText: '',
};

export default SearchTimeboxes;