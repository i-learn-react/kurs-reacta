import React from 'react';
import PropTypes from 'prop-types';

const Timebox = ({
    title, 
    totalTimeInMinutes, 
    handleDelete, 
    handleEdit
}) => {
    if (totalTimeInMinutes <= 0) {
        throw new Error('Time should be more than 0');
    }
    return (
        <div className='Timebox'>
            <h3>{title} - {totalTimeInMinutes}</h3>
            <button onClick={handleDelete} >Usuń</button>
            <button onClick={handleEdit} >Edytuj</button>

        </div>
    );
}

Timebox.propTypes = {
    title: PropTypes.string.isRequired,
    totalTimeInMinutes: PropTypes.string.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
};

Timebox.defaultProps = {
    title: '',
    totalTimeInMinutes: 0,
    handleDelete: () => console.log('Add handleDelete prop'),
    handleEdit: () => console.log('Add handleEdit prop'),
};

export { Timebox };