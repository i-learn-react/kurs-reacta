import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function ProgressBar({
    percent,
    trackRemaining,
    className,
    isPaused,
    isBig,
    color,
}) {
    const barStyle = trackRemaining
        ? { width: `${100 - percent}%` }
        : { width: `${percent}%` };
    const progressBarStyle = classNames(
        className,
        'progress',
        {
            'progress--big': isBig,
            'progress--regular': !isBig,
            'inactive': isPaused,
            'progress--right-aligned': trackRemaining,
            'progress--left-aligned': !trackRemaining,
        }
    );
    const barClassName = classNames(
        'progress__bar',
        `progress--${color}`,
    );

    return (
        <div className={progressBarStyle}>
            <div className={barClassName} style={barStyle}></div>
        </div>
    )
};

const isPercent = (props, propName, componentName) => {
    if (props[propName] && (!Boolean(props[propName]) || props[propName] < 0 || props[propName] > 100))
        return new Error(`Invalid prop ${propName} passed to ${componentName}. Expected number between 0 and 100`)
}

ProgressBar.propTypes = {
    percent: isPercent,
    trackRemaining: PropTypes.bool.isRequired,
    isPaused: PropTypes.bool.isRequired,
    isBig: PropTypes.bool.isRequired,
    className: PropTypes.string.isRequired,
    color: PropTypes.oneOf(['orange', 'red', 'green']),
};

ProgressBar.defaultProps = {
    percent: 0,
    trackRemaining: false,
    isPaused: true,
    isBig: false,
    className: '',
    color: 'orange',
};

export { ProgressBar };