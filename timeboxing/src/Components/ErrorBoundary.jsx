import React from 'react';
import PropTypes from 'prop-types';

import ErrorMessage from './ErrorMessage';

class ErrorBoundary extends React.Component {
    state = {
        hasError: false,
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        console.log('Something is wrong: ', error, errorInfo);
    }

    render() {
        const { message, children } = this.props;
        return (
            <ErrorMessage hasError={this.state.hasError} message={message} >
                {children}
            </ErrorMessage>
        )
    }
}

ErrorBoundary.propTypes = {
    message: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

ErrorBoundary.defaultProps = {
    message: '',
    children: null,
}


export default ErrorBoundary;