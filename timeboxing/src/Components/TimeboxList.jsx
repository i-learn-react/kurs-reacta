import React from 'react';

import TimeboxCreator from './TimeboxCreator';
import { Timebox } from './Timebox';
import ErrorBoundary from './ErrorBoundary';
import ErrorMessage from './ErrorMessage';
import createTimeboxesAPI from '../api/AxiosTimeboxesAPI';
import SearchTimeboxes from './SearchTimeboxes';

const TimeboxesAPI = createTimeboxesAPI({ baseUrl: 'http://localhost:3001/timeboxes' });

class TimeboxList extends React.Component {
    state = {
        timeboxes: [],
        hasError: false,
        errorMessage: '',
        loading: true,
        searchText: '',
    };

    componentDidMount() {
        TimeboxesAPI.getAllTimeboxes(this.props.accessToken)
            .then(timeboxes => this.setState({ timeboxes }))
            .catch(error => this.setState({
                hasError: true,
                errorMessage: error.message,
            }))
            .finally(() => this.setState({ loading: false }));
    }
    

    addTimebox = (timebox) => {
        if (!timebox.title || !timebox.totalTimeInMinutes) {
            throw new Error('Method addTimebox failed!');
        }
        TimeboxesAPI.addTimebox(timebox, this.props.accessToken)
            .then(timeboxAdded =>
                this.setState(prevState => (
                    { timeboxes: [...prevState.timeboxes, timeboxAdded] }
                ))
            );
    }
    
    deleteTimebox = timeboxToRemove => {
        TimeboxesAPI.deleteTimebox(timeboxToRemove, this.props.accessToken)
            .then(() => 
                this.setState(prevState => {
                    const filteredTimeboxes = prevState.timeboxes.filter(timebox => timebox.id !== timeboxToRemove.id);
                    return { ...prevState, timeboxes: [...filteredTimeboxes] };
                })
            )
    }

    editTimebox = timeboxToUpdate => {
        const timeboxEdited = { id: timeboxToUpdate.id, title: 'Updated Title' };
        TimeboxesAPI.partiallyUpdateTimebox(timeboxEdited, this.props.accessToken)
            .then(timeboxUpdated =>
                    this.setState(prevState => {
                        const updatedTimeboxes = prevState.timeboxes.map(timebox =>
                            timebox.id === timeboxUpdated.id
                                ? timeboxUpdated
                                : timebox
                        );
                        return ({ ...prevState, timeboxes: [...updatedTimeboxes] }) 
                    })
            )
    }
    
    handleDelete = (timeboxToRemove) => this.deleteTimebox(timeboxToRemove);
    
    handleEdit = (timeboxToEdit) => this.editTimebox(timeboxToEdit);
    
    handleCreate = (createdTimebox) => {
        try {
            this.addTimebox({ ...createdTimebox });
        } catch (error) {
            this.setState({ hasError: true, errorMessage: `Error: ${error.message}!` });
        }
    }
    handleSearch = (event) => {
        event.preventDefault();
        TimeboxesAPI.getTimeboxesByFullTextSearch(this.state.searchText, this.props.accessToken)
            .then(timeboxesFound =>
                this.setState(prevState => ({ ...prevState, timeboxes: [...timeboxesFound] }))
            );
    }
    handleSearchInput = (event) => {
        const searchText = event.target.value;
        this.setState(prevState => ({...prevState, searchText }))
    }
    handleClearSearch = (event) => {
        event.preventDefault();
        this.setState({ searchText: '' });
    }

    render() {
        const { timeboxes, loading, hasError, errorMessage, searchText } = this.state;
        return (
            <React.Fragment>
                <ErrorMessage hasError={hasError} message={errorMessage} >
                    <TimeboxCreator
                        handleCreate={this.handleCreate}
                    />
                </ErrorMessage>
                {loading ? <h2>Loading…</h2> : null}
                <SearchTimeboxes
                    handleSearchInput={this.handleSearchInput}
                    handleClearSearch={this.handleClearSearch}
                    handleSearch={this.handleSearch}
                    searchText={searchText}
                />
                <ErrorBoundary message='Ups. This should work.'>
                    {
                        timeboxes.map(timebox => (
                            <Timebox
                                key={timebox.id}
                                title={timebox.title}
                                totalTimeInMinutes={timebox.totalTimeInMinutes}
                                handleDelete={() => this.handleDelete(timebox)}
                                handleEdit={() => this.handleEdit(timebox)}
                            />
                        ))
                    }
                </ErrorBoundary>
            </React.Fragment >
        )
    }
}

export default TimeboxList;