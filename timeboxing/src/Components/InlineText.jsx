import React from 'react';
import PropTypes from 'prop-types';

const InlineText = ({
    children,
    className = '',
}) => {
    return (
        <span className={className}>
            {children}
        </span>
    );
};

InlineText.propTypes = {
    className: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

InlineText.defaultProps = {
    className: '',
    children: null,
}

export default InlineText;