import React from 'react';

import { TimeboxEditor } from './TimeboxEditor';
import CurrentTimebox from './CurrentTimebox';


class EditableTimebox extends React.Component {
    state = {
        title: 'Foo',
        totalTimeInMinutes: 1,
        isEditable: false,
    }
    handleTitleChange = (event) => {
        this.setState({
            title: event.target.value,
        })
    }
    handleTotalTimeInMinutesChange = (event) => {
        this.setState({
            totalTimeInMinutes: event.target.value,
        })
    }
    handleConfirm = (event) => {
        this.setState({
            isEditable: false,
        })
    }
    handleEdit = (event) => {
        this.setState({
            isEditable: true,
        })
    }
    render() {
        const { title, totalTimeInMinutes, isEditable } = this.state;
        return (
            <React.Fragment>
                {isEditable
                    ? (
                        <TimeboxEditor
                            handleTitleChange={this.handleTitleChange}
                            handleTotalTimeInMinutesChange={this.handleTotalTimeInMinutesChange}
                            title={title}
                            totalTimeInMinutes={totalTimeInMinutes}
                            isEditable={isEditable}
                            onConfirm={this.handleConfirm}
                        />
                    )
                    : (
                        <CurrentTimebox
                            title={title}
                            totalTimeInMinutes={totalTimeInMinutes}
                            isEditable={isEditable}
                            onEdit={this.handleEdit}
                        />
                    )
                }
            </React.Fragment>
        )
    }
}

export default EditableTimebox;