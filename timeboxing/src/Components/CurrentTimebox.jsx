import React from 'react';
import PropTypes from 'prop-types';

import { increasePausesCount } from '../Helpers/increasePausesCount';
import { Clock } from './Clock';
import { ProgressBar } from './ProgressBar';

class CurrentTimebox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRunning: false,
            isPaused: false,
            pausesCount: 0,
            elapsedTimeInSeconds: 0,
        }
        this.handleStart = this.handleStart.bind(this)
        this.handleStop = this.handleStop.bind(this)
        this.togglePause = this.togglePause.bind(this)
        this.intervalId = null;
    }

    componentWillUnmount() {
        this.stopTimer();
    }

    handleStart() {
        this.setState(prevState => (
            {
                isRunning: true,
            }
        ));
        this.startTimer();
    }

    handleStop(event) {
        this.setState(prevState => (
            {
                isRunning: false,
                isPaused: false,
                pausesCount: 0,
                elapsedTimeInSeconds: 0,
            }
        ));
        this.stopTimer();
    }

    togglePause() {
        this.setState(function (prevState) {
            prevState.isPaused ? this.startTimer() : this.stopTimer();
            return {
                isPaused: !prevState.isPaused,
                pausesCount: increasePausesCount(prevState.pausesCount, prevState.isPaused),
            }
        });
    }

    startTimer() {
        if (!this.intervalId) {
            this.intervalId = window.setInterval(() => {
                this.setState(prevState => (
                    {
                        elapsedTimeInSeconds: prevState.elapsedTimeInSeconds + 1
                    }
                ))
            }, 1000);
        }
    }

    stopTimer() {
        window.clearInterval(this.intervalId);
        this.intervalId = null;
    }

    render() {
        const { title, totalTimeInMinutes, isEditable, onEdit } = this.props;
        const { isRunning, isPaused, pausesCount, elapsedTimeInSeconds } = this.state
        const pauseText = isPaused ? 'Kontynuuj' : 'Pauzuj';
        const totalTimeInSeconds = totalTimeInMinutes * 60;
        const remainedTimeInSeconds = totalTimeInSeconds - elapsedTimeInSeconds;
        const remainedMinutes = Math.floor(remainedTimeInSeconds / 60);
        const remainedSeconds = remainedTimeInSeconds % 60;
        const progressInPercent = Math.floor(elapsedTimeInSeconds / totalTimeInSeconds * 100);
        const finalClassName = isEditable ? 'CurrentTimebox inactive' : 'CurrentTimebox';
        return (
            <div className={finalClassName}>
                <h1>{title}</h1>
                <Clock
                    minutes={remainedMinutes}
                    seconds={remainedSeconds}
                    isPaused={isPaused}
                />
                <ProgressBar
                    percent={progressInPercent}
                    trackRemaining
                    isPaused={isPaused}
                />
                <button onClick={this.handleStart} disabled={isRunning}>Start</button>
                <button onClick={onEdit} disabled={isEditable}>Edytuj</button>
                <button onClick={this.handleStop} disabled={!isRunning}>Stop</button>
                <button onClick={this.togglePause} disabled={!isRunning}>{pauseText}</button>
                <span className='Breaks'>Liczba przerw: {pausesCount}</span>
            </div>
        )
    }
}

CurrentTimebox.propTypes = {
    title: PropTypes.string.isRequired,
    totalTimeInMinutes: PropTypes.number.isRequired,
    isEditable: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
};

CurrentTimebox.defaultProps = {
    title: '',
    totalTimeInMinutes: 0,
    isEditable: false,
    onEdit: () => null
};

export default CurrentTimebox;