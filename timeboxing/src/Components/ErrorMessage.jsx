import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = ({ hasError, message, children }) =>
    hasError
        ? <div className="ErrorMessage">
            {message}
        </div>
        : children;

ErrorMessage.propTypes = {
    hasError: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

ErrorMessage.defaultProps = {
    hasError: false,
    message: '',
    children: null,
}

export default ErrorMessage;