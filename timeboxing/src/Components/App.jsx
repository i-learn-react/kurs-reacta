import React, { Fragment } from 'react';
import jwt from 'jsonwebtoken';

import TimeboxList from './TimeboxList';
import EditableTimebox from './EditableTimebox';
import ErrorBoundary from './ErrorBoundary';
import LoginForm from './LoginForm';
import createAuthenticationAPI from '../api/AxiosAuthenticationAPI';
import {
    saveAccessToken,
    clearAccessToken,
    getAccessToken,
    saveSessionEndtime,
    getSessionEndtime,
    clearSessionEndtime,
} from '../Helpers/storage';

const AuthenticationAPI = createAuthenticationAPI({ baseUrl: 'http://localhost:3001' });
const SECOND = 1000;
const SESSION_TIME = 3600 * SECOND;

const getCurrentTime = () => Date.now();

class App extends React.Component {
    constructor(props) {
        super(props)
        this.sessionTimer = null;
    }
    state = {
        isLoggedIn: false,
        errorMessage: '',
        accessToken: null,
        previousLoginAttemptFailed: false,
        currentTime: 0,
    }

    handleLoginAttempt = (credentials) => {
        AuthenticationAPI.login(credentials)
            .then(({ accessToken }) => {
                const currentTime = getCurrentTime();
                saveAccessToken(accessToken);
                saveSessionEndtime(currentTime+SESSION_TIME);
                this.setState(
                    {
                        isLoggedIn: true,
                        accessToken,
                        previousLoginAttemptFailed: false,
                        currentTime,
                    }
                );
                this.startTimer();
            })
            .catch(error => this.setState(
                {
                    previousLoginAttemptFailed: true,
                    errorMessage: error.message,
                })
            );
    }

    handleLogout = () => {
        this.stopTimer();
        clearAccessToken();
        clearSessionEndtime();
        this.setState(
            {
                isLoggedIn: false,
                accessToken: null,
                previousLoginAttemptFailed: false,
                currentTime: 0,
            }
        );
    };

    isUserLoggedIn = () => Boolean(this.state.accessToken);

    startTimer = () => this.sessionTimer = window.setInterval(this.checkTimeout, 1000);
    stopTimer = () => window.clearInterval(this.sessionTimer);

    checkTimeout = () => {
        const currentTime = getCurrentTime();
        if (currentTime >= getSessionEndtime()) {
            this.handleLogout()
        } else {
            this.addSecond()
        }
    }
    addSecond = () => this.setState(prevState => (
        { currentTime: prevState.currentTime + SECOND })
    );

    getUserEmail = () => {
        const decoded = jwt.decode(this.state.accessToken);
        return decoded.email;
    };

    componentDidMount() {
        const currentTime = getCurrentTime();
        if (currentTime < getSessionEndtime()) {
            this.startTimer();
            this.setState({
                accessToken: getAccessToken(),
                currentTime,
            })
        };
    }
    
    render() {
        return (
            <div className='App'>
                <React.StrictMode>
                    <ErrorBoundary message='App Level error. We will fix that'>
                        {
                            this.isUserLoggedIn()
                                ? (
                                    <Fragment>
                                        <header className='Header' >
                                            <p>Witaj {this.getUserEmail()}</p>
                                            <button onClick={this.handleLogout} className='Header__logout'>Wyloguj się</button>
                                        </header>
                                        <TimeboxList accessToken={this.state.accessToken}/>
                                        <EditableTimebox />                                    
                                    </Fragment>
                                )
                                : (
                                    <LoginForm
                                        hasError={this.state.previousLoginAttemptFailed}
                                        errorMessage={this.state.errorMessage}
                                        handleLoginAttempt={this.handleLoginAttempt}
                                    />
                                )
                        }
                    </ErrorBoundary>
                </React.StrictMode>
            </div>
        );
    }
}

export default App;


