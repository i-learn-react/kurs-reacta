import React from 'react';
import PropTypes from 'prop-types';

const TimeboxEditor = ({
    title,
    totalTimeInMinutes,
    isEditable,
    handleTitleChange,
    handleTotalTimeInMinutesChange,
    onConfirm,
}) => {
    const finalClassName = isEditable ? 'TimeboxEditor' : 'TimeboxEditor inactive';
    const buttonClassName = isEditable ? 'FormField' : 'FormField inactive';
    return (
        <div className={finalClassName}>
            <label className={buttonClassName}>
                <span>Co robisz?</span>
                <input
                    value={title}
                    type='text'
                    name='what'
                    onChange={handleTitleChange}
                />
            </label>
            <label className={buttonClassName}>
                <span>Ile minut?</span>
                <input
                    value={totalTimeInMinutes}
                    type='number'
                    name='time'
                    onChange={handleTotalTimeInMinutesChange}
                />
            </label>
            <button
                disabled={!isEditable}
                onClick={onConfirm}
            >
                Zatwierdź zmiany
                </button>
        </div>
    )
};

TimeboxEditor.propTypes = {
    title: PropTypes.string.isRequired,
    totalTimeInMinutes: PropTypes.number.isRequired,
    isEditable: PropTypes.bool.isRequired,
    handleTitleChange: PropTypes.func.isRequired,
    handleTotalTimeInMinutesChange: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
};

TimeboxEditor.defaultProps = {
    title: '',
    totalTimeInMinutes: 0,
    isEditable: false,
    handleTitleChange: () => null,
    handleTotalTimeInMinutesChange: () => null,
    onConfirm: () => null,
};


export { TimeboxEditor };