import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import InlineText from './InlineText';

function Clock({
    hours,
    minutes, 
    seconds, 
    milliseconds, 
    className,
    isPaused,
}) {
    const zeroPad = (digits, value = '') => value.padStart(digits, '0');
    const padTwo = (number = 0) => zeroPad(2, number.toString());
    const padThree = (number = 0) => zeroPad(3, number.toString());
    const normalize = (min, max, value) => {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    };
    const normalizeHours = hours => padTwo(normalize(0, 24, hours));
    const normalizeMinutes = minutes => padTwo(normalize(0, 59, minutes));
    const normalizeSeconds = seconds => padTwo(normalize(0, 59, seconds));
    const normalizeMilliseconds = milliseconds => padThree(normalize(0, 999, milliseconds));
    const clockClassName = classNames(
        'clock',
        className,
        { 'inactive': isPaused }
    );
    return (
        <h2 className={clockClassName}>
            <InlineText>Pozostało </InlineText>
            <InlineText className='clock__hours'>{normalizeHours(hours)}</InlineText>
            <InlineText className='clock__separator'>:</InlineText>
            <InlineText className='clock__minutes'>{normalizeMinutes(minutes)}</InlineText>
            <InlineText className='clock__separator'>:</InlineText>
            <InlineText className='clock__seconds'>{normalizeSeconds(seconds)}</InlineText>
            <InlineText className='clock__separator'>:</InlineText>
            <InlineText className='clock__milliseconds'>{normalizeMilliseconds(milliseconds)}</InlineText>
        </h2>
    )
}

Clock.propTypes = {
    hours: PropTypes.number.isRequired,
    minutes: PropTypes.number.isRequired,
    seconds: PropTypes.number.isRequired,
    milliseconds: PropTypes.number.isRequired,
    className: PropTypes.string.isRequired,
    isPaused: PropTypes.bool.isRequired,
}

Clock.defaultProps = {
    hours: 0,
    minutes: 0,
    seconds: 0,
    milliseconds: 0,
    className: '',
    isPaused: false,
}

export { Clock };