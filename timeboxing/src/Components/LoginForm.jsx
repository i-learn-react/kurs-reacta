import React, { Fragment } from 'react';
import ErrorMessage from './ErrorMessage';

const loginFormDefaultState = {
    email: '',
    password: '',
}

class LoginForm extends React.Component {
    state = { ...loginFormDefaultState }
    clearForm = () => {
        this.setState({ ...loginFormDefaultState });
    }
    handleSubmit = (event) => {
        event.preventDefault();
        this.props.handleLoginAttempt({
            email: this.state.email,
            password: this.state.password,
        });
        this.clearForm();
    }
    handleEmailChange = (event) => {
        this.setState({ email: event.target.value });
    }
    handlePasswordChange = (event) => {
        this.setState({ password: event.target.value });
    }
    render() {
        const { email, password } = this.state;
        return (
            <Fragment>
                <ErrorMessage hasError={this.props.hasError} message={this.props.errorMessage}>
                    <header className='Header' >
                        <p>Witaj</p>
                    </header>
                </ErrorMessage>
                <form
                    onSubmit={this.handleSubmit}
                    className='LoginForm'
                    ref={this.form}
                >
                    <label className='FormField'>
                        <span>Email</span>
                        <input
                            value={email}
                            onChange={this.handleEmailChange}
                            type='text'
                            name='email'
                            placeholder='bob@example.com'
                        />
                    </label>
                    <label className='FormField'>
                        <span>Password</span>
                        <input
                            value={password}
                            onChange={this.handlePasswordChange}
                            type='password'
                            name='password'
                            placeholder='your password'
                        />
                    </label>
                    <button>
                        Zaloguj się
                    </button>
                </form >
            </Fragment>
        )
    }
};

export default LoginForm;