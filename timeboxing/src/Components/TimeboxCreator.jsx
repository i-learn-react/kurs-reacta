import React from 'react';

const timeboxCreatorDefaultState = {
    title: '',
    totalTimeInMinutes: '',
}

class TimeboxCreator extends React.Component {
    state = { ...timeboxCreatorDefaultState }
    clearForm = () => {
        this.setState({ ...timeboxCreatorDefaultState });
    }
    handleSubmit = (event) => {
        event.preventDefault();
        this.props.handleCreate({
            title: this.state.title,
            totalTimeInMinutes: this.state.totalTimeInMinutes,
        });
        this.clearForm();
    }
    handleTitleChange = (event) => {
        this.setState({ title: event.target.value });
    }
    handleTotalTimeInMinutesChange = (event) => {
        this.setState({ totalTimeInMinutes: event.target.value });
    }
    render() {
        const { title, totalTimeInMinutes } = this.state;
        return (
            <form
                onSubmit={this.handleSubmit}
                className='TimeboxCreator'
                ref={this.form}
            >
                <label className='FormField'>
                    <span>Co robisz?</span>
                    <input
                        value={title}
                        onChange={this.handleTitleChange}
                        type='text'
                        name='what'
                    />
                </label>
                <label className='FormField'>
                    <span>Ile minut?</span>
                    <input
                        value={totalTimeInMinutes}
                        onChange={this.handleTotalTimeInMinutesChange}
                        type='number'
                        name='time'
                    />
                </label>
                <button>
                    Dodaj timebox
                </button>
            </form >
        )
    }
};

export default TimeboxCreator;